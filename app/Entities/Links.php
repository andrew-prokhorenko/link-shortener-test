<?php

namespace App\Entities;

use Illuminate\Database\Eloquent\Model;

/**
 * Class LinksResource
 * @package App\Entities
 * @property int id
 * @property string long
 * @property string short
 * @property string generated_slug
 * @property bool active
 * @property int created_at
 * @property int updated_at
 */
class Link extends Model
{
    protected $fillable = ['long', 'short', 'active'];

    protected $casts = [
        'long' => 'string',
        'short' => 'string',
        'active' => 'boolean'
    ];
}
