<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreLink;
use App\Http\Resources\LinksResource;
use App\Repositories\Interfaces\LinkRepositoryInterface;

class LinkApiController extends Controller
{
    private $linkRepository;

    public function __construct(LinkRepositoryInterface $linkRepository)
    {
        $this->linkRepository = $linkRepository;
    }

    public function store(StoreLink $request)
    {
        $validated = $request->validated();

        $link = $this->linkRepository->create($validated);

        return LinksResource::make($link);
    }
}
