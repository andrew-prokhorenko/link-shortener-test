<?php

namespace App\Http\Controllers;

use App\Repositories\Interfaces\LinkRepositoryInterface;

class LinkController extends Controller
{
    private $linkRepository;

    public function __construct(LinkRepositoryInterface $linkRepository)
    {
        $this->linkRepository = $linkRepository;
    }

    public function redirect()
    {
        $link = $this->linkRepository->getByShortLink(url()->current());

        if ($link->active) {
            return redirect($link->long);
        }

        return abort(404);
    }
}
