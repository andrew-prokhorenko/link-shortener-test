<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreLink extends FormRequest
{
    public function authorize() : bool
    {
        return true;
    }

    public function rules() : array
    {
        return [
            'long' => 'required|url|string|max:255',
            'short' => 'string|url|max:255',
            'active' => 'boolean'
        ];
    }
}
