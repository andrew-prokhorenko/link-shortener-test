<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use \Illuminate\Http\Request;
use App\Entities\Link;

/**
 * @property Link $resource
 */
class LinksResource extends JsonResource
{
    /**
     * @param Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->resource->id,
            'long' => $this->resource->long,
            'short' => $this->resource->short,
            'active' => $this->resource->active,
            'created_at_formatted' => $this->resource->created_at->format('h:i, d M Y'),
        ];
    }
}
