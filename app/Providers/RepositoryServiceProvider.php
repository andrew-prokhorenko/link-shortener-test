<?php

namespace App\Providers;

use App\Repositories\LinkRepository;
use App\Repositories\Interfaces\LinkRepositoryInterface;
use Illuminate\Support\ServiceProvider;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     */
    public function register() : void
    {
        $this->app->bind(
            LinkRepositoryInterface::class,
            LinkRepository::class
        );
    }

    /**
     * Bootstrap services.
     */
    public function boot() : void
    {
        //
    }
}
