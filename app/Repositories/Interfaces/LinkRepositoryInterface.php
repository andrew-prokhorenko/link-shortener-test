<?php

namespace App\Repositories\Interfaces;

interface LinkRepositoryInterface
{
    public function create(array $data);

    public function getByShortLink(string $link);

    public function existsWithSlug(string $slug);
}
