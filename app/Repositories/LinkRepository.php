<?php

namespace App\Repositories;

use Illuminate\Database\Eloquent\Model;
use App\Entities\Link;
use App\Repositories\Interfaces\LinkRepositoryInterface;

class LinkRepository implements LinkRepositoryInterface
{
    public function create(array $data) : Link
    {
        $slug = \Str::random(6);

        $link = new Link($data);

        if (!$this->existsWithSlug($slug)) {
            $link->short = route('home') . '/' . $slug;
            $link->save();
        }

        return $link;
    }

    public function getByShortLink(string $link) : Model
    {
        return Link::query()->where('short', $link)->firstOrFail();
    }

    public function existsWithSlug(string $slug)
    {
        return Link::query()->where('short', route('home') . '/' . $slug)->exists();
    }
}
