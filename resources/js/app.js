require('./bootstrap');

window.validate = require('validate.js');
window.Vue = require('vue');

import URLForm from "./components/URLForm";

new Vue({
    el: '#app',
    components: {
       'url-form': URLForm
    }
});
