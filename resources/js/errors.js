const errors =  {
  incorrectUrl: 'The provided link is incorrect',
  alreadyInUse: 'This link is already in use',
};

export default errors;
