@extends('layouts.app')

@section('content')
    <div class="container">
        <h1 class="text-center font-weight-bold">URL Shortener</h1>
        <url-form>
            @csrf
        </url-form>
    </div>
@endsection
