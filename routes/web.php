<?php

Route::get('/', function () {
    return view('home');
})->name('home');

Route::get('/{any}', 'LinkController@redirect')->name('redirect');
